import { Component } from "react";
import Dice from "./components/dice/dice";

class App extends Component{
  render() {
    return(
      <div className="p-5">
        <Dice/>
      </div>
    )
  }
}
export default App;
