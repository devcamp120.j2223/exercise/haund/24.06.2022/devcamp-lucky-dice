import { Component } from "react";

class Input extends Component {
    onBtnThrowDice = () => {
        console.log("Clicked");
        this.props.outputChangeHandleProp();
    }


    render(){
        return(
            <div>
                <div className="row">
                    <button onClick={this.onBtnThrowDice} className="btn btn-success" style={{width: "100px"}} > Throw Dice </button>
                </div>
            </div>
        )
    }
}

export default Input;