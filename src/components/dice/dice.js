import { Component } from "react";
import Input from "./input/input";
import Output from "./uotput/output";

import imgDice from "../../assets/images/dice.png";
import value1 from "../../assets/images/1.png";
import value2 from "../../assets/images/2.png";
import value3 from "../../assets/images/3.png";
import value4 from "../../assets/images/4.png";
import value5 from "../../assets/images/5.png";
import value6 from "../../assets/images/6.png";

class Dice extends Component {
    constructor(props) {
        super(props);

        this.state = {imgResult: imgDice, valueDice: "Ấn vào nút tung xúc xắc (Kết quả sẽ hiển thị ở đây)"}
    }

    outputchangeHandle = () =>{
        let number = Math.floor((Math.random() * 6) + 1);
        let result = null;

        switch (number) {
            case 1: 
            result = value1
            break;

            case 2: 
            result = value2
            break;
            
            case 3: 
            result = value3
            break;

            case 4: 
            result = value4
            break;

            case 5: 
            result = value5
            break;

            case 6: 
            result = value6
            break;
        }
        console.log(number);
        this.setState({
            imgResult: result,
            valueDice: "kết quả là: " + number
        })
        
    }



    render(){
        return(
            <>
                <Output imgResultProp = {this.state.imgResult} valueDiceProp = {this.state.valueDice} />
                <Input outputChangeHandleProp = {this.outputchangeHandle}/>            
            </>
        )
    }
}

export default Dice;