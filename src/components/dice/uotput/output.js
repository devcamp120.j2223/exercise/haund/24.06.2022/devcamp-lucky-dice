import { Component } from "react";

class Output extends Component {
    render(){
        const { imgResultProp, valueDiceProp } = this.props;
        return(
            <div>
                <div className="row">
                    <img src={imgResultProp} alt="dice-start" style={{width: "150px"}} />
                </div>
                <div className="row">
                    <p>{valueDiceProp}</p>
                </div>
            </div>
        )
    }
}

export default Output;